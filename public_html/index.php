<!DOCTYPE html>
<html>
	<head>
		<title>Pebble Interactive - Mobile Design Agency</title>
		<meta http-equiv="Content-Type"  content="text/html; charset=utf-8" >
		<meta name="keywords" content="pebble, pebble interactive, mobile, design, mobile design, mobile development, mobile apps, small screen, interactions, applications, websites, mobile web, responsive, cms, seo, ipad, iphone, ios, Android, Windows Phone, WP7, ux, user experience, brighton, london, east sussex, cyclist, mens fitness, evo magazine" />
		<meta name="description" content="Pebble Interactive - An award winning Mobile Design Agency based in Brighton, UK" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
		<meta name="format-detection" content="telephone=no" />
		<link rel="stylesheet" href="/assets/css/pebble.css" type="text/css" media="screen" charset="utf-8" />
		<script type="text/javascript" src="//use.typekit.net/wav3mgr.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
		<script src="//code.jquery.com/jquery-latest.min.js"></script>
		<script src="//unslider.com/unslider.min.js"></script>
		<script>
			$(function() {
			    //$('.gallery_image_wrapper').unslider();
				$('.gallery_image_wrapper').unslider({
					speed: 500,               //  The speed to animate each slide (in milliseconds)
					delay: 3000,              //  The delay between slide animations (in milliseconds)
					complete: function() {},  //  A function that gets called after every slide animation
					keys: true,               //  Enable keyboard (left, right) arrow shortcuts
					dots: true,               //  Display dot navigation
					fluid: true              //  Support responsive design. May break non-responsive designs
				});
				
			});
		</script>
		<style>
			
		</style>
	</head>
	<body>
		<div class="wrapper">

			<section class="header_intro">
				<header>
				
				</header>
				<section class="intro tk-ff-meta-web-pro">
					<div class="content">
					<h2 class="tk-lexia">Making mobile simple</h2>
					<p>
					We specialise in mobile design, and have created applications and web experiences for iOS, Android &amp; Windows Phone - for big brands and small startups. Let us help you create the best experience wherever your clients&nbsp;are, and whatever device they&nbsp;choose.
					</p>
					</div>
				</section>
				<div class="clear"></div>
			</section>
			
			<section id="work">

				<!-- Project Start -->
				<section class="project" id="dennis_publishing_evo_magazine">
					<h2 class="tk-lexia"><b>evo</b> for iPhone, iPad, Android</h2>
					<div class="gallery">
						<div class="gallery_image_wrapper">
							<ul>
								<li class="gallery_image" style="background-image:url('/dynamic/evo/iphone/home_feed.jpg');"></li>
								<li class="gallery_image" style="background-image:url('/dynamic/evo/iphone/driven_twin_test.jpg');"></li>
								<li class="gallery_image" style="background-image:url('/dynamic/evo/iphone/art_of_speed.jpg');"></li>
								<li class="gallery_image" style="background-image:url('/dynamic/evo/iphone/knowledge.jpg');"></li>
							</ul>
						</div>
					</div>
					<div class="description tk-ff-meta-web-pro">
						<h3>0-60 in under 3 seconds.</h3>
						<p>We worked with the speed mad team at evo magazine to make their content available for multiple platforms. It wasn't too hard with some kick ass car photography at our fingertips.</p>
						
						<h3>What we did for evo</h3>
						<ul>
							<li>CMS</li>
							<li>HTML5</li>
							<li>CSS</li>
							<li>Responsive design</li>
						</ul>
					</div>
					<div class="description_after"></div>
				</section>
				<!-- Project End -->
				<!-- Project Start -->
				<section class="project" id="dennis_publishing_mensfitness_magazine">
					<h2 class="tk-lexia"><b>Men's Fitness Magazine</b> for iPhone, iPad, Android</h2>
					<div class="gallery">
						<div class="gallery_image_wrapper">
							<ul>
								<li class="gallery_image" style="background-image:url('/dynamic/mf/iphone/food_1.jpg');"></li>
								<li class="gallery_image" style="background-image:url('/dynamic/mf/iphone/workout_1.jpg');"></li>
							</ul>
						</div>
					</div>
					<div class="description tk-ff-meta-web-pro">
						<h3>Drop and give me 20.</h3>
						<p>Working with muscle men can be intimidating. However, the Men's Fitness Publishing team we're more than happy when we said we could make their content available on multiple platforms.</p>
						<h3>What we did for Men's Fitness</h3>
						<ul>
							<li>CMS</li>
							<li>HTML5</li>
							<li>CSS</li>
							<li>Responsive design</li>
						</ul>
					</div>
					<div class="description_after"></div>
				</section>
				<!-- Project End -->
				<!-- Project Start -->
				<section class="project" id="dennis_publishing_cyclist_magazine">
					<h2 class="tk-lexia"><b>Cyclist Magazine</b> for iPhone, iPad &amp; Android</h2>
					<div class="gallery">
						<div class="gallery_image_wrapper">
							<ul>
								<li class="gallery_image" style="background-image:url('/dynamic/cyclist/iphone/splash.png');"></li>
							</ul>
						</div>
					</div>
					<div class="description tk-ff-meta-web-pro">
						<h3>Ride on</h3>
						<p>Road cyclists are passionate about their sport. Pebble are passionate about technology. This was the first magazine we took cross platform, so we needed to combine our passions to make the best on the market. With 4.5 stars on the App Store, we did pretty well.</p>
						<h3>What we did for Cyclist</h3>
						<ul>
							<li>CMS</li>
							<li>HTML5</li>
							<li>CSS</li>
							<li>Responsive design</li>
						</ul>

					</div>
					<div class="description_after"></div>
				</section>
				<!-- Project End -->
				<!-- Project Start -->
				<section class="project" id="dennis_publishing_profanisaurus_ipad_app">
					<h2 class="tk-lexia"><b>Viz Profanisaurus App</b> for iPad</h2>
					<div class="gallery">
						<div class="gallery_image_wrapper">
							<ul>
								<li class="gallery_image" style="background-image:url('/dynamic/viz/pages_1.png');"></li>
								<li class="gallery_image" style="background-image:url('/dynamic/viz/pages_2.png');"></li>
							</ul>
						</div>
					</div>
					<div class="description tk-ff-meta-web-pro">
						<h3>Phnarr phnarr</h3>
						<p>It's always good to have a sense of humour. So when we had the opportunity to create a Viz app specifically for iPad, we couldn't refuse. With a tongue in cheek nod to the printed edition, as well as custom illustrations, we had fun making this one.
						</p>
						<h3>What we did for Viz Profanisaurus</h3>
						<ul>
							<li>Wireframes</li>
							<li>Visual Design</li>
							<li>Asset production</li>
						</ul>
					</div>
					<div class="description_after"></div>
				</section>
				<!-- Project End -->
				<!-- Project Start -->
				<section class="project" id="ee_glastonbury_2013_app">
					<h2 class="tk-lexia"><b>Glastonbury Festival App</b> for iOS &amp; Android</h2>
					<div class="gallery">
						<div class="gallery_image_wrapper">
							<ul>
								<li class="gallery_image" style="background-image:url('/dynamic/glasto/iphone/splash.png');"></li>
								<li class="gallery_image" style="background-image:url('/dynamic/glasto/iphone/map_home.png');"></li>
								<li class="gallery_image" style="background-image:url('/dynamic/glasto/iphone/map_recharge.png');"></li>
							</ul>
						</div>
					</div>
					<div class="description tk-ff-meta-web-pro">
						<h3>Party on</h3>
						<p>
							Creating an app for 150k+ festival goers is always going to be a challenge. We took the maps and line ups, added some magic and the party goers found their way around. 
						</p>
						<h3>What we did for Glastonbury Festival</h3>
						<ul>
							<li>Wireframes</li>
							<li>Visual Design</li>
							<li>Asset production</li>
						</ul>
					</div>
					<div class="description_after"></div>
				</section>
				<!-- Project End -->
				<!-- Project Start -->
				<section class="project" id="johnlewis_windows8_app">
					<h2 class="tk-lexia"><b>John Lewis App</b> for Windows 8</h2>
					<div class="gallery">
						<div class="gallery_image_wrapper">
							<ul>
								<li class="gallery_image" style="background-image:url('/dynamic/johnlewis/pages_1.png');"></li>
								<li class="gallery_image" style="background-image:url('/dynamic/johnlewis/pages_2.png');"></li>
							</ul>
						</div>
					</div>
					<div class="description tk-ff-meta-web-pro">
						<h3>Discerning shopping</h3>
						<p>
							John Lewis wanted a presence on the Windows 8 platform. It simply offers their pick of products from their online store, presented beautifully for the Windows 8 platform.
						</p>
						<h3>What we did for John Lewis</h3>
						<ul>
							<li>Wireframes</li>
							<li>Visual Design</li>
							<li>Asset production</li>
						</ul>

					</div>
					<div class="description_after"></div>
				</section>
				<!-- Project End -->
								
			</section>
			<section class="contact tk-ff-meta-web-pro">
				<h2 class="tk-lexia">We are passionate about mobile</h2>
				<p>
				Drop us a line and let’s take it from&nbsp;there.
				</p>
				<a class="button" href="mailto:hello@pebbleinteractive.com">send us an email</a>
			</section>
			
			<footer>
			
			</footer>
		</div>
	</body>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-41271341-1', 'pebbleinteractive.com');
	  ga('send', 'pageview');

	</script>
</html>